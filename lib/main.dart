
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'UserWidget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(title: 'Fluser'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  TextEditingController nameCtrl,passwordCtrl;
  @override
  void initState(){
    super.initState();
    nameCtrl = new TextEditingController();
    passwordCtrl = new TextEditingController();
  }
  String user = "";
  List<User> users = List<User>();
  String errorTexts=".";
  int i=0;
  void _removeUser() {
    setState(() {
      if(users.isEmpty) {
        Fluttertoast.showToast(
            msg: "user List empty", toastLength: Toast.LENGTH_SHORT);
        }else{
          if((nameCtrl.text==null) || (passwordCtrl.text == null)){
            Fluttertoast.showToast(msg: "All Fields are required",toastLength: Toast.LENGTH_SHORT);
          }else{
            User user1 = User(nameCtrl.text,passwordCtrl.text);
            if(!users.contains(user1)){
            users.removeWhere((user) => user.name==user1.name&&user.password==user1.password);
            }else
              Fluttertoast.showToast(msg: "User does not exist..",toastLength: Toast.LENGTH_SHORT);
        }}});
  }

  void _addUser(){
    setState(() {
      if((nameCtrl.text.length==0) || (passwordCtrl.text == null)){
        Fluttertoast.showToast(msg: "All Fields are required",toastLength: Toast.LENGTH_SHORT);
        }else if(passwordCtrl.text.length<6) {
        Fluttertoast.showToast(
            msg: "password too short, should be atleast 6 characters..",
            toastLength: Toast.LENGTH_SHORT);
      }else{
          User user = new User(nameCtrl.text,passwordCtrl.text);
        if(!users.isEmpty && users.contains(user)) {
          Fluttertoast.showToast(msg: "Already exists !!",toastLength: Toast.LENGTH_SHORT);
          }else if(!users.contains(user)){
            Fluttertoast.showToast(msg: "User "+user.name+" added successfully..",toastLength: Toast.LENGTH_SHORT);
            users.add(user);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image:AssetImage("assets/fluserbg.png"),
            fit: BoxFit.cover),
      ),
        child:Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children:<Widget>[
          Container(
            child: Stack(
              children: <Widget>[
               Container(
                padding: EdgeInsets.fromLTRB(15.0, 110.0, 0.0, 0.0),
                child: Text(
                  'Fluser',
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,),
                    )
                  )
                ],
              ),
          ),
            Container(
              padding: EdgeInsets.only(top: 35.0,left:20.0,right: 20.0),
                child: Column(
                  children: <Widget>[
                    TextField(
                      controller: nameCtrl,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.account_box,),
                          labelText: 'Name',
                          labelStyle: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black38
                          ),
                        focusedBorder: UnderlineInputBorder(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20.0),
                                bottomRight: Radius.circular(20.0)),
                            borderSide: BorderSide(color: Colors.orangeAccent),
                            ),
                      ),
                    ),
                    SizedBox(height: 5.0,),
                    TextField(
                      controller: passwordCtrl,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.lock),
                          labelText: 'password',
                          labelStyle: TextStyle(
                                 fontWeight: FontWeight.bold,
                                 color: Colors.black38,
                          ),
                          focusedBorder: UnderlineInputBorder(
                                borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(20.0),
                                        bottomRight: Radius.circular(20.0)),
                                borderSide: BorderSide(color: Colors.orangeAccent),
                                ),
                      ),
                       obscureText: true,
                    ),
                    SizedBox(height: 5.0,),
                    Container(
                      alignment: Alignment(1.0,0.0),
                      padding: EdgeInsets.only(top: 15,left: 20.0),
                      child: InkWell(
                        child: Text('Forgot Password',
                          style: TextStyle(
                            color: Colors.black38,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline
                            ),),
                      ),
                      ),
                  SizedBox(height: 40.0,),
                  Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children:<Widget> [
                            ElevatedButton(
                              onPressed: _addUser,
                              child: Text('Add User'),
                              ),
                            ElevatedButton(
                              onPressed: _removeUser,
                              child: Text('Remove User'),
                              ),
                            ],
                        ),
                      ),
                    SizedBox(
                      height: 70,
                      child: UserList(users: users)
                    )
                  ],
                ),
              ),
          ]
      )
    ));
  }
}
class User {
  final String name;
  final String password;
  User(this.name,this.password);
}