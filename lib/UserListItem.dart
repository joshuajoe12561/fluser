
import 'dart:ui';

import 'package:fluser/main.dart';
import 'package:flutter/material.dart';

typedef void UserChangedCallback(User user, bool exists);

class UserListItem extends StatelessWidget {
  UserListItem({this.user, this.exists, this.onUserChanged})
      : super(key: ObjectKey(user));

  final User user;
  final bool exists;
  final UserChangedCallback onUserChanged;

  Color _getColor(BuildContext context) {
    return exists ? Colors.black54 : Theme.of(context).primaryColor;
  }

  TextStyle _getTextStyle(BuildContext context) {
    if (!exists) return null;

    return TextStyle(
      color: Colors.black54,
      decoration: TextDecoration.lineThrough,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        onUserChanged(user, exists);
      },
      leading: CircleAvatar(
        backgroundColor: _getColor(context),
        child: Text(user.name[0]),
      ),
      title: Text(user.name, style: _getTextStyle(context)),
    );
  }
}
