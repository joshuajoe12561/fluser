import 'package:fluser/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'UserListItem.dart';

class UserList extends StatefulWidget {
  UserList({Key key, this.users}) : super(key: key);

  final List<User> users;

  // The framework calls createState the first time a widget
  // appears at a given location in the tree.
  // If the parent rebuilds and uses the same type of
  // widget (with the same key), the framework re-uses the State object
  // instead of creating a new State object.

  @override
  _UserListState createState() => _UserListState();
}

class _UserListState extends State<UserList> {
  Set<User> _userList = Set<User>();

  void _handleUserChanged(User user, bool exists) {
    setState(() {

      if (!exists)
        _userList.add(user);
      else
        _userList.remove(user);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.symmetric(vertical: 8.0),
        children: widget.users.map((User user) {
          return UserListItem(
            user: user,
            exists: _userList.contains(user),
            onUserChanged: _handleUserChanged,
          );
        }).toList(),
      ),
    );
  }
}